import boto3
import os


aws_access_key_id = os.environ.get('AWS_ACCESS_KEY_ID')
aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
region_name = os.environ.get('AWS_DEFAULT_REGION')

session = boto3.Session(
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key
)

dynamodb = session.resource('dynamodb', region_name=region_name)
client = boto3.client('dynamodb')
existingtables = client.list_tables()


if 'WatchlistSettings' not in existingtables['TableNames']:
    table = dynamodb.create_table(
        TableName='WatchlistSettings',
        AttributeDefinitions=[
            {
                'AttributeName': 'userId',
                'AttributeType': 'S'
            }
        ],
        KeySchema=[
            {
                'AttributeName': 'userId',
                'KeyType': 'HASH'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 10,
            'WriteCapacityUnits': 10
        }
    )

    # Wait until the table exists.
    table.meta.client.get_waiter('table_exists').wait(
        TableName='WatchlistSettings'
    )
    print('table created')
else:
    print('table already exists')
