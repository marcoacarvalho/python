#!/usr/bin/env python
# -*- coding: utf-8 -*-

import boto3

client = boto3.client("ec2")

def item_sort(elem):
    return elem.get('StartTime')

snapshots = client.describe_snapshots(OwnerIds=['self'])['Snapshots']
snapshots.sort(key=item_sort)

uat_daily = 0
uat_weekly = 0
uat_daily_count = 0
uat_weekly_count = 0
prod_daily = 0
prod_weekly = 0
prod_daily_count = 0
prod_weekly_count = 0
qa_daily = 0
qa_weekly = 0
qa_daily_count = 0
qa_weekly_count = 0

for snapshot in snapshots:

    instance_name = ''
    backup_plan = ''
    if "Tags" in snapshot:
        snapshot_size = snapshot['VolumeSize']
        for tag in snapshot['Tags']:
            if tag['Key'] == "Name":
                instance_name = tag["Value"]
            if tag['Key'] == 'Backup_plan':
                backup_plan = tag['Value']
                

        if "uat" in instance_name:
            if backup_plan == 'Daily':
                uat_daily = uat_daily + snapshot_size
                uat_daily_count = uat_daily_count +1
            if backup_plan == 'Weekly':
                uat_weekly = uat_weekly + snapshot_size
                uat_weekly_count = uat_weekly_count +1
            print(f"{instance_name} | {snapshot_size} | {backup_plan}")      

        if "prod" in instance_name:
            if backup_plan == 'Daily':
                prod_daily = prod_daily + snapshot_size
                prod_daily_count = prod_daily_count +1
            if backup_plan == 'Weekly':
                prod_weekly = prod_weekly + snapshot_size
                prod_weekly_count = prod_weekly_count +1
            print(f"{instance_name} | {snapshot_size} | {backup_plan}")

        if "qa" in instance_name:
            if backup_plan == 'Daily':
                qa_daily = qa_daily + snapshot_size
                qa_daily_count = qa_daily_count +1
            if backup_plan == 'Weekly':
                qa_weekly = qa_weekly + snapshot_size
                qa_weekly_count = qa_weekly_count +1
            print(f"{instance_name} | {snapshot_size} | {backup_plan}")

print(f"QA Daily: {qa_daily_count} snapshots ({qa_daily}Gb)")
print(f"QA Weekly: {qa_weekly_count} snapshots ({qa_weekly}Gb)")

print(f"UAT Daily: {uat_daily_count} snapshots ({uat_daily}Gb)")
print(f"UAT Weekly: {uat_weekly_count} snapshots ({uat_weekly}Gb)")

print(f"PROD Daily: {prod_daily_count} snapshots ({prod_daily}Gb)")
print(f"PROD Weekly: {prod_weekly_count} snapshots ({prod_weekly}Gb)")