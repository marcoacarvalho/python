#!/usr/bin/env python
# -*- coding: utf-8 -*-

import boto3

client = boto3.client("backup")

backup_jobs = client.list_backup_jobs(ByState='COMPLETED')['BackupJobs']

uat_total = 0
prod_total = 0
qa_total = 0

for job in backup_jobs:
    resource_name = job['ResourceName']
    backup_size = job['BackupSizeInBytes']
    backup_size_human = int(backup_size / (1024 ** 3))
    print(f"{resource_name} | {backup_size_human} Gb")
    if "dragfiuat" in resource_name:
        uat_total = uat_total + backup_size_human
    if "dragfiprod" in resource_name:
        prod_total = prod_total + backup_size_human
    if "dragfiqa" in resource_name:
        qa_total = qa_total + backup_size_human

print(f"PROD: {prod_total} Gb")
print(f"UAT: {uat_total} Gb")
print(f"QA: {qa_total} Gb")