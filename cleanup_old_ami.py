#!/usr/bin/env python
# -*- coding: utf-8 -*-

import boto3
from datetime import datetime
from dateutil.parser import parse

current_date=datetime.now()

client = boto3.client("ec2")

def image_sort(elem):
    return elem.get('CreationDate')

my_ami = client.describe_images(Owners=['self'])['Images']
my_ami.sort(key=image_sort)

amis_to_deregister = []

ami_count=0
snapshot_count=0

for ami in my_ami:
    creation_date=ami['CreationDate']
    creation_date_parse=parse(creation_date).replace(tzinfo=None)
    ami_id = ami['ImageId']
    ami_name = ami['Name']
    ami_ebss = ami['BlockDeviceMappings']
    ami_tags = "No Backup plan"
    instance_name = ""
    if "Tags" in ami:
        ami_tags = ami['Tags']
        for tag in ami_tags:
            if tag['Key'] == "Backup_plan":
                ami_tags = f'Backup_plan {tag["Value"]}'
            if tag['Key'] == "Name":
                instance_name = tag["Value"]
    else:
        ami_tags = "No tags"
    if ami_tags == "No tags":
        amis_to_deregister.append({"creation_date":creation_date, "ami_id":ami_id, "ami_name":ami_name, "ami_tags":ami_tags, "instance_name":instance_name, "ami_ebss":ami_ebss})

for ami in amis_to_deregister:
    ami_count = ami_count + 1
    print(f'{ami["creation_date"]} | {ami["ami_id"]} | {ami["ami_name"]} | {ami["ami_tags"]} | {ami["instance_name"]}')
    for ebs in ami["ami_ebss"]:
        if 'Ebs' in ebs:
            snapshot_id = ebs['Ebs']['SnapshotId']
            snapshot_count = snapshot_count + 1
            print(snapshot_id)

print(f"We're about to remove {ami_count} AMIs and {snapshot_count} snapshots")
proceed = ""
while proceed != "Y" and proceed != "N":
    proceed = input("Would you like to proceed (Y/N)? ").upper()
    if proceed == "Y":
        for ami in amis_to_deregister:
            print('-----------------------------------------------------------------------------')
            print(f'Deregistering AMI {ami["ami_id"]}')
            #print(f'{ami["creation_date"]} | {ami["ami_id"]} | {ami["ami_name"]} | {ami["ami_tags"]} | {ami["instance_name"]}')
            client.deregister_image(ImageId = ami["ami_id"])
            for ebs in ami["ami_ebss"]:
                if 'Ebs' in ebs:
                    snapshot_id = ebs['Ebs']['SnapshotId']
                    print(f'Deleting snapshot {snapshot_id}')
                    client.delete_snapshot(SnapshotId=snapshot_id)
    else:
        break