"""Application exporter"""

import os
import time
import sys
import websocket
import logging
import ecs_logging
from prometheus_client import start_http_server, Gauge, Enum

class WsMetrics:
    """
    Representation of Prometheus metrics and loop to fetch and transform
    application metrics into Prometheus metrics.
    """

    def __init__(self, endpointlist, polling_interval_seconds=5):
        self.logger = logging.getLogger("prometheus-ws-exporter")
        self.logger.setLevel(logging.INFO)
        handler = logging.FileHandler('prometheus-ws-exporter.json')
        streamHandler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(ecs_logging.StdlibFormatter())
        streamHandler.setFormatter(ecs_logging.StdlibFormatter())
        self.logger.addHandler(handler)
        self.logger.addHandler(streamHandler)
        self.polling_interval_seconds = polling_interval_seconds
        # Prometheus metrics to collect
        self.ws_liveness={}
        self.ws_lenght={}
        self.endpointlist = endpointlist
        for endpoint in self.endpointlist:
            endpoint_url = endpoint.split("?")[0]
            endpoint_clean = endpoint_url.replace("wss://", "").replace("ws://", "").replace("/", "_").replace(".", "_").replace("-","_")
            self.ws_liveness[endpoint_clean] = Gauge('endpoint_alive_%s' % endpoint_clean, 'Endpoint liveness')
            self.ws_lenght[endpoint_clean] = Gauge('endpoint_length_%s' % endpoint_clean, 'Endpoint length')


    def run_metrics_loop(self):
        """Metrics fetching loop"""

        while True:
            self.fetch()
            time.sleep(self.polling_interval_seconds)

    def fetch(self):
        """
        Get metrics from websockets and refresh Prometheus metrics with
        new values.
        """

        # Fetch raw status data from the websockets and
        # Update Prometheus metrics with websockets metrics
        for endpoint in self.endpointlist:
            endpoint_url = endpoint.split("?")[0]
            endpoint_clean = endpoint_url.replace("wss://", "").replace("ws://", "").replace("/", "_").replace(".", "_").replace("-","_")
            websocket.enableTrace(False)
            ws = websocket.WebSocket()
            try:
                ws.connect(endpoint)
                self.ws_liveness[endpoint_clean].set("1")
                msg = ws.recv_data()
                self.ws_lenght[endpoint_clean].set(len(msg[1]))
                message = msg[1].decode()
                self.logger.info(message, extra={"websocket_msg_length_kb": len(msg[1]), "websocket_endpoint": endpoint_url})
            except websocket._exceptions.WebSocketBadStatusException:
                self.ws_liveness[endpoint_clean].set("0")
                self.logger.error("Failled to connect websocket", extra={"endpoint": endpoint_url})

def main():
    """Main entry point"""

    polling_interval_seconds = int(os.getenv("POLLING_INTERVAL_SECONDS", "5"))
    endpointlist = os.getenv("WS_ENDPOINTS", "ws://ws.ifelse.io").split(",")
    exporter_port = int(os.getenv("EXPORTER_PORT", "9877"))

    ws_metrics = WsMetrics(
        polling_interval_seconds=polling_interval_seconds,
        endpointlist=endpointlist
    )
    start_http_server(exporter_port)
    ws_metrics.run_metrics_loop()

if __name__ == "__main__":
    main()