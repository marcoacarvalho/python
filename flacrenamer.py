#!/usr/bin/python
from mutagen.flac import FLAC
import sys
import os


def parse(s):
    fmt = ''
    para = []
    flag = False
    for i in s:
        if flag:
            try:
                para.append({
                    'p': 'artist',
                    't': 'title',
                    'a': 'album',
                    'n': 'tracknumber'
                }[i])
                fmt += '%s'
            except KeyError:
                if i == '%':
                    fmt += '%'
                fmt += i
            flag = False
        else:
            if i == '%':
                flag = True
            else:
                fmt += i
    return fmt, para


def main():
    tagger = {
        '.flac': FLAC,
    }
    fmtstr = '%p/%a/%n %t'
    dirstr = '%p/%a'
    path = '/home/marco/temp/x'
    file_list = filter((lambda x: '.flac' in x), os.listdir(path))
    os.chdir(path)
    fmtstr, para = parse(fmtstr)
    dirstr, para2 = parse(dirstr)
    for filename in file_list:
        filename = os.path.abspath(filename)
        dirname = os.path.dirname(filename)
        extension = os.path.splitext(filename)[1].lower()
        if not os.path.isfile(filename):
            print('Cannot find such file: ' + filename)
            continue
        try:
            audio = tagger[extension](filename)
        except KeyError:
            print('Cannot deal with such type of file: ' + extension)
            continue
        newname = dirname + '/'
        newdir = dirname + '/'
        newname += fmtstr % tuple([audio.get(i, [''])[0] for i in para])
        newdir += dirstr % tuple([audio.get(i, [''])[0] for i in ['artist', 'album']])
        newname += extension
        newdir = newdir.replace(' / ', ' - ')
        newname = newname.replace(' / ', ' - ')
        print(newdir)
        print('Processing: ' + newname)
        if not os.path.exists(newdir):
            os.makedirs(newdir)
        os.rename(filename, newname)


if __name__ == '__main__':
    main()

